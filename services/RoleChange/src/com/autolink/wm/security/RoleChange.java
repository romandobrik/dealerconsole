/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.autolink.wm.security;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import java.util.*;
//import com.autolink.wm.security.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class RoleChange {

    private static final Logger logger = LoggerFactory.getLogger(RoleChange.class);

    @Autowired
    private SecurityService securityService;

    
public String addRole(String role){

    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    //Collection<GrantedAuthority> authorities = new ArrayList<>(authentication.getAuthorities());
    Collection<GrantedAuthority> authorities = new ArrayList<>();
     //authorities.remove(new SimpleGrantedAuthority("user"));
    authorities.add(new SimpleGrantedAuthority(role));
    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = 
           new UsernamePasswordAuthenticationToken(
                    authentication.getPrincipal(), authentication.getCredentials(), authorities);
    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    
    return role;

}    

}
