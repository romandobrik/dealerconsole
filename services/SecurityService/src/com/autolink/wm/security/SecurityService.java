/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/

package com.autolink.wm.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.JSONObject;

import java.util.List;
import java.util.ArrayList;
 
import javax.servlet.http.HttpServletRequest;
 
import com.wavemaker.runtime.security.WMCustomAuthenticationManager;
import com.wavemaker.runtime.security.AuthRequestContext;
import com.wavemaker.runtime.security.WMUser;

import org.springframework.security.core.AuthenticationException;

import com.autolink.security.stormpath.StormpathService;
import com.autolink.security.stormpath.StormpathService.User;
import com.autolink.security.stormpath.StormpathException;

public class SecurityService implements WMCustomAuthenticationManager {

    private static final Logger logger=LoggerFactory.getLogger(SecurityService.class);
    
    StormpathService stormpathService = new StormpathService();

    @Override
    public WMUser authenticate(AuthRequestContext context) throws AuthenticationException {
            String username = context.getUsername();
            String password = context.getPassword();
            
            HttpServletRequest httpContext = context.getHttpServletRequest();
            //javax.servlet.ServletContext servletContext = httpContext.getServletContext();
            String dealerId = httpContext.getParameter("dealerid");
            
            java.util.Enumeration names = httpContext.getParameterNames();
            while(names.hasMoreElements())
                logger.error(names.nextElement().toString());
            
           if(dealerId == null){
            logger.error("dealer is null");
             // return null;
           }
            
		try {
			User user = stormpathService.authenticate(username, password);
            List roles = new ArrayList();
            
            for(String item : user.groups.keySet())
                roles.add(item);
            

            //if(!roles.contains(dealerId))
            //    return null;
            //Integer.parseInt(dealerId)    
            return new WMUser(user.id, user.email, password, user.fullName, 851425, roles);
            
			
		} catch (StormpathException e) {
			return null;
		}            

    }
}
