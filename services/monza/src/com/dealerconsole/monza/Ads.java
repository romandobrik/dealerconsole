/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

/**
 * Ads generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`ads`", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"`ad_id`"})})
@IdClass(AdsId.class)
public class Ads implements Serializable {

    private Integer adId;
    private Integer dealerId;
    private int campaignId;
    private String name;
    private Integer type;
    private String id;
    private String description;
    private Integer year;
    private String make;
    private String model;
    private String trim;
    private String vin;
    private Integer discount;
    @Type(type = "DateTime")
    private LocalDateTime expiration;
    private Integer state;
    @Type(type = "DateTime")
    private LocalDateTime created;
    @Type(type = "DateTime")
    private LocalDateTime modified;
    private String modifiedby;

    @Id
    @Column(name = "`ad_id`", nullable = false, scale = 0, precision = 10)
    public Integer getAdId() {
        return this.adId;
    }

    public void setAdId(Integer adId) {
        this.adId = adId;
    }

    @Id
    @Column(name = "`dealer_id`", nullable = false, scale = 0, precision = 10)
    public Integer getDealerId() {
        return this.dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    @Column(name = "`campaign_id`", nullable = false, scale = 0, precision = 10)
    public int getCampaignId() {
        return this.campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    @Column(name = "`name`", nullable = true, length = 45)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "`type`", nullable = true, scale = 0, precision = 10)
    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name = "`id`", nullable = true, length = 60)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "`description`", nullable = true, length = 200)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "`year`", nullable = true, scale = 0, precision = 10)
    public Integer getYear() {
        return this.year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Column(name = "`make`", nullable = true, length = 45)
    public String getMake() {
        return this.make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Column(name = "`model`", nullable = true, length = 45)
    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Column(name = "`trim`", nullable = true, length = 45)
    public String getTrim() {
        return this.trim;
    }

    public void setTrim(String trim) {
        this.trim = trim;
    }

    @Column(name = "`vin`", nullable = true, length = 45)
    public String getVin() {
        return this.vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Column(name = "`discount`", nullable = true, scale = 0, precision = 10)
    public Integer getDiscount() {
        return this.discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    @Column(name = "`expiration`", nullable = true)
    public LocalDateTime getExpiration() {
        return this.expiration;
    }

    public void setExpiration(LocalDateTime expiration) {
        this.expiration = expiration;
    }

    @Column(name = "`state`", nullable = true, scale = 0, precision = 10)
    public Integer getState() {
        return this.state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Column(name = "`created`", nullable = true)
    public LocalDateTime getCreated() {
        return this.created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Column(name = "`modified`", nullable = true)
    public LocalDateTime getModified() {
        return this.modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Column(name = "`modifiedby`", nullable = true, length = 60)
    public String getModifiedby() {
        return this.modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ads)) return false;
        final Ads ads = (Ads) o;
        return Objects.equals(getAdId(), ads.getAdId()) &&
                Objects.equals(getDealerId(), ads.getDealerId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAdId(),
                getDealerId());
    }
}

