/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.dealerconsole.monza.MatchTiers;


/**
 * ServiceImpl object for domain model class MatchTiers.
 *
 * @see MatchTiers
 */
@Service("monza.MatchTiersService")
public class MatchTiersServiceImpl implements MatchTiersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchTiersServiceImpl.class);


    @Autowired
    @Qualifier("monza.MatchTiersDao")
    private WMGenericDao<MatchTiers, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<MatchTiers, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "monzaTransactionManager")
    @Override
	public MatchTiers create(MatchTiers matchTiers) {
        LOGGER.debug("Creating a new MatchTiers with information: {}", matchTiers);
        MatchTiers matchTiersCreated = this.wmGenericDao.create(matchTiers);
        return matchTiersCreated;
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public MatchTiers getById(Integer matchtiersId) throws EntityNotFoundException {
        LOGGER.debug("Finding MatchTiers by id: {}", matchtiersId);
        MatchTiers matchTiers = this.wmGenericDao.findById(matchtiersId);
        if (matchTiers == null){
            LOGGER.debug("No MatchTiers found with id: {}", matchtiersId);
            throw new EntityNotFoundException(String.valueOf(matchtiersId));
        }
        return matchTiers;
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public MatchTiers findById(Integer matchtiersId) {
        LOGGER.debug("Finding MatchTiers by id: {}", matchtiersId);
        return this.wmGenericDao.findById(matchtiersId);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public MatchTiers getByDealerIdAndTierAndMakeAndModel(int dealerId, int tier, String make, String model) {
        Map<String, Object> dealerIdAndTierAndMakeAndModelMap = new HashMap<>();
        dealerIdAndTierAndMakeAndModelMap.put("dealerId", dealerId);
        dealerIdAndTierAndMakeAndModelMap.put("tier", tier);
        dealerIdAndTierAndMakeAndModelMap.put("make", make);
        dealerIdAndTierAndMakeAndModelMap.put("model", model);

        LOGGER.debug("Finding MatchTiers by unique keys: {}", dealerIdAndTierAndMakeAndModelMap);
        MatchTiers matchTiers = this.wmGenericDao.findByUniqueKey(dealerIdAndTierAndMakeAndModelMap);

        if (matchTiers == null){
            LOGGER.debug("No MatchTiers found with given unique key values: {}", dealerIdAndTierAndMakeAndModelMap);
            throw new EntityNotFoundException(String.valueOf(dealerIdAndTierAndMakeAndModelMap));
        }

        return matchTiers;
    }

	@Transactional(rollbackFor = EntityNotFoundException.class, value = "monzaTransactionManager")
	@Override
	public MatchTiers update(MatchTiers matchTiers) throws EntityNotFoundException {
        LOGGER.debug("Updating MatchTiers with information: {}", matchTiers);
        this.wmGenericDao.update(matchTiers);

        Integer matchtiersId = matchTiers.getMatchTierId();

        return this.wmGenericDao.findById(matchtiersId);
    }

    @Transactional(value = "monzaTransactionManager")
	@Override
	public MatchTiers delete(Integer matchtiersId) throws EntityNotFoundException {
        LOGGER.debug("Deleting MatchTiers with id: {}", matchtiersId);
        MatchTiers deleted = this.wmGenericDao.findById(matchtiersId);
        if (deleted == null) {
            LOGGER.debug("No MatchTiers found with id: {}", matchtiersId);
            throw new EntityNotFoundException(String.valueOf(matchtiersId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public Page<MatchTiers> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all MatchTiers");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<MatchTiers> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all MatchTiers");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service monza for table MatchTiers to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

