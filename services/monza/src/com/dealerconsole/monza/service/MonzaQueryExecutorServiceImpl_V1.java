/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/

package com.dealerconsole.monza.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.dealerconsole.monza.models.query.*;

@Service
public class MonzaQueryExecutorServiceImpl_V1 implements MonzaQueryExecutorService_V1 {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonzaQueryExecutorServiceImpl.class);

    @Autowired
    @Qualifier("monzaWMQueryExecutor")
    private WMQueryExecutor queryExecutor;

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executeUsersByDealerId(Pageable pageable, Integer dealerId) {
        Map params = new HashMap(1);

        params.put("dealerId", dealerId);

        return queryExecutor.executeNamedQuery("UsersByDealerId", params, Object.class, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executeSelectedUser(Pageable pageable, String username, Integer dealerId) {
        Map params = new HashMap(2);

        params.put("username", username);
        params.put("dealerId", dealerId);

        return queryExecutor.executeNamedQuery("SelectedUser", params, Object.class, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executeDealerDiscounts(Pageable pageable, Integer dealerId, Integer year, String make, String model, String trim) {
        Map params = new HashMap(5);

        params.put("dealerId", dealerId);
        params.put("year", year);
        params.put("make", make);
        params.put("model", model);
        params.put("trim", trim);

        return queryExecutor.executeNamedQuery("DealerDiscounts", params, Object.class, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executePriceByVin(Pageable pageable, String vin, Integer dealerId) {
        Map params = new HashMap(2);

        params.put("vin", vin);
        params.put("dealerId", dealerId);

        return queryExecutor.executeNamedQuery("PriceByVin", params, Object.class, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executePriceDiscounts(Pageable pageable, Integer dealerId) {
        Map params = new HashMap(1);

        params.put("dealerId", dealerId);

        return queryExecutor.executeNamedQuery("PriceDiscounts", params, Object.class, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executeDealerRebates(Pageable pageable, Integer dealerId, Integer year, String make, String model, String trim) {
        Map params = new HashMap(5);

        params.put("dealerId", dealerId);
        params.put("year", year);
        params.put("make", make);
        params.put("model", model);
        params.put("trim", trim);

        return queryExecutor.executeNamedQuery("DealerRebates", params, Object.class, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executeDealerInfo(Pageable pageable, Integer id) {
        Map params = new HashMap(1);

        params.put("id", id);

        return queryExecutor.executeNamedQuery("DealerInfo", params, Object.class, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Object> executeGetUserByDealers(Pageable pageable, List<Integer> dealers, String username) {
        Map params = new HashMap(2);

        params.put("dealers", dealers);
        params.put("username", username);

        return queryExecutor.executeNamedQuery("GetUserByDealers", params, Object.class, pageable);
    }

}


