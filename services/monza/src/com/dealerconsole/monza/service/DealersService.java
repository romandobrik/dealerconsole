/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.dealerconsole.monza.ChannelDiscount;
import com.dealerconsole.monza.Channels;
import com.dealerconsole.monza.DealerMake;
import com.dealerconsole.monza.DealerRebates;
import com.dealerconsole.monza.DealerZipcode;
import com.dealerconsole.monza.Dealers;
import com.dealerconsole.monza.DiscountAgeRanges;
import com.dealerconsole.monza.MatchTiers;
import com.dealerconsole.monza.Notifications;
import com.dealerconsole.monza.PriceSetup;
import com.dealerconsole.monza.Prices;
import com.dealerconsole.monza.Users;

/**
 * Service object for domain model class {@link Dealers}.
 */
public interface DealersService {

    /**
     * Creates a new Dealers. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Dealers if any.
     *
     * @param dealers Details of the Dealers to be created; value cannot be null.
     * @return The newly created Dealers.
     */
	Dealers create(Dealers dealers);


	/**
	 * Returns Dealers by given id if exists.
	 *
	 * @param dealersId The id of the Dealers to get; value cannot be null.
	 * @return Dealers associated with the given dealersId.
     * @throws EntityNotFoundException If no Dealers is found.
	 */
	Dealers getById(Integer dealersId) throws EntityNotFoundException;

    /**
	 * Find and return the Dealers by given id if exists, returns null otherwise.
	 *
	 * @param dealersId The id of the Dealers to get; value cannot be null.
	 * @return Dealers associated with the given dealersId.
	 */
	Dealers findById(Integer dealersId);


	/**
	 * Updates the details of an existing Dealers. It replaces all fields of the existing Dealers with the given dealers.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Dealers if any.
     *
	 * @param dealers The details of the Dealers to be updated; value cannot be null.
	 * @return The updated Dealers.
	 * @throws EntityNotFoundException if no Dealers is found with given input.
	 */
	Dealers update(Dealers dealers) throws EntityNotFoundException;

    /**
	 * Deletes an existing Dealers with the given id.
	 *
	 * @param dealersId The id of the Dealers to be deleted; value cannot be null.
	 * @return The deleted Dealers.
	 * @throws EntityNotFoundException if no Dealers found with the given id.
	 */
	Dealers delete(Integer dealersId) throws EntityNotFoundException;

	/**
	 * Find all Dealers matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Dealers.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Dealers> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Dealers matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Dealers.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Dealers> findAll(String query, Pageable pageable);

    /**
	 * Exports all Dealers matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Dealers in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Dealers.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated channelDiscounts for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated ChannelDiscount instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<ChannelDiscount> findAssociatedChannelDiscounts(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated channelses for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated Channels instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<Channels> findAssociatedChannelses(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated dealerMakes for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated DealerMake instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<DealerMake> findAssociatedDealerMakes(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated dealerRebateses for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated DealerRebates instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<DealerRebates> findAssociatedDealerRebateses(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated dealerZipcodes for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated DealerZipcode instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<DealerZipcode> findAssociatedDealerZipcodes(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated discountAgeRangeses for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated DiscountAgeRanges instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<DiscountAgeRanges> findAssociatedDiscountAgeRangeses(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated matchTierses for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated MatchTiers instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<MatchTiers> findAssociatedMatchTierses(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated notificationses for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated Notifications instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<Notifications> findAssociatedNotificationses(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated priceSetups for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated PriceSetup instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<PriceSetup> findAssociatedPriceSetups(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated priceses for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated Prices instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<Prices> findAssociatedPriceses(Integer dealerId, Pageable pageable);

    /*
     * Returns the associated userses for given Dealers id.
     *
     * @param dealerId value of dealerId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated Users instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<Users> findAssociatedUserses(Integer dealerId, Pageable pageable);

}

