/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.dealerconsole.monza.Notifications;
import com.dealerconsole.monza.NotificationsId;


/**
 * ServiceImpl object for domain model class Notifications.
 *
 * @see Notifications
 */
@Service("monza.NotificationsService")
public class NotificationsServiceImpl implements NotificationsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsServiceImpl.class);


    @Autowired
    @Qualifier("monza.NotificationsDao")
    private WMGenericDao<Notifications, NotificationsId> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Notifications, NotificationsId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "monzaTransactionManager")
    @Override
	public Notifications create(Notifications notifications) {
        LOGGER.debug("Creating a new Notifications with information: {}", notifications);
        Notifications notificationsCreated = this.wmGenericDao.create(notifications);
        return notificationsCreated;
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public Notifications getById(NotificationsId notificationsId) throws EntityNotFoundException {
        LOGGER.debug("Finding Notifications by id: {}", notificationsId);
        Notifications notifications = this.wmGenericDao.findById(notificationsId);
        if (notifications == null){
            LOGGER.debug("No Notifications found with id: {}", notificationsId);
            throw new EntityNotFoundException(String.valueOf(notificationsId));
        }
        return notifications;
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public Notifications findById(NotificationsId notificationsId) {
        LOGGER.debug("Finding Notifications by id: {}", notificationsId);
        return this.wmGenericDao.findById(notificationsId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "monzaTransactionManager")
	@Override
	public Notifications update(Notifications notifications) throws EntityNotFoundException {
        LOGGER.debug("Updating Notifications with information: {}", notifications);
        this.wmGenericDao.update(notifications);

        NotificationsId notificationsId = new NotificationsId();
        notificationsId.setNotificationId(notifications.getNotificationId());
        notificationsId.setDealerId(notifications.getDealerId());

        return this.wmGenericDao.findById(notificationsId);
    }

    @Transactional(value = "monzaTransactionManager")
	@Override
	public Notifications delete(NotificationsId notificationsId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Notifications with id: {}", notificationsId);
        Notifications deleted = this.wmGenericDao.findById(notificationsId);
        if (deleted == null) {
            LOGGER.debug("No Notifications found with id: {}", notificationsId);
            throw new EntityNotFoundException(String.valueOf(notificationsId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public Page<Notifications> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Notifications");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<Notifications> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Notifications");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service monza for table Notifications to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

