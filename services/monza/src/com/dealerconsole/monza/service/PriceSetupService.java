/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.dealerconsole.monza.ChannelDiscount;
import com.dealerconsole.monza.DiscountAgeRanges;
import com.dealerconsole.monza.PriceSetup;

/**
 * Service object for domain model class {@link PriceSetup}.
 */
public interface PriceSetupService {

    /**
     * Creates a new PriceSetup. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on PriceSetup if any.
     *
     * @param priceSetup Details of the PriceSetup to be created; value cannot be null.
     * @return The newly created PriceSetup.
     */
	PriceSetup create(PriceSetup priceSetup);


	/**
	 * Returns PriceSetup by given id if exists.
	 *
	 * @param pricesetupId The id of the PriceSetup to get; value cannot be null.
	 * @return PriceSetup associated with the given pricesetupId.
     * @throws EntityNotFoundException If no PriceSetup is found.
	 */
	PriceSetup getById(Integer pricesetupId) throws EntityNotFoundException;

    /**
	 * Find and return the PriceSetup by given id if exists, returns null otherwise.
	 *
	 * @param pricesetupId The id of the PriceSetup to get; value cannot be null.
	 * @return PriceSetup associated with the given pricesetupId.
	 */
	PriceSetup findById(Integer pricesetupId);


	/**
	 * Updates the details of an existing PriceSetup. It replaces all fields of the existing PriceSetup with the given priceSetup.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on PriceSetup if any.
     *
	 * @param priceSetup The details of the PriceSetup to be updated; value cannot be null.
	 * @return The updated PriceSetup.
	 * @throws EntityNotFoundException if no PriceSetup is found with given input.
	 */
	PriceSetup update(PriceSetup priceSetup) throws EntityNotFoundException;

    /**
	 * Deletes an existing PriceSetup with the given id.
	 *
	 * @param pricesetupId The id of the PriceSetup to be deleted; value cannot be null.
	 * @return The deleted PriceSetup.
	 * @throws EntityNotFoundException if no PriceSetup found with the given id.
	 */
	PriceSetup delete(Integer pricesetupId) throws EntityNotFoundException;

	/**
	 * Find all PriceSetups matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PriceSetups.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<PriceSetup> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all PriceSetups matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PriceSetups.
     *
     * @see Pageable
     * @see Page
	 */
    Page<PriceSetup> findAll(String query, Pageable pageable);

    /**
	 * Exports all PriceSetups matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the PriceSetups in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the PriceSetup.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated channelDiscounts for given PriceSetup id.
     *
     * @param priceSetupId value of priceSetupId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated ChannelDiscount instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<ChannelDiscount> findAssociatedChannelDiscounts(Integer priceSetupId, Pageable pageable);

    /*
     * Returns the associated discountAgeRangeses for given PriceSetup id.
     *
     * @param priceSetupId value of priceSetupId; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated DiscountAgeRanges instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<DiscountAgeRanges> findAssociatedDiscountAgeRangeses(Integer priceSetupId, Pageable pageable);

}

