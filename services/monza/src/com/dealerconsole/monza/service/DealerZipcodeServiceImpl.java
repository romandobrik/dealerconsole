/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.dealerconsole.monza.DealerZipcode;


/**
 * ServiceImpl object for domain model class DealerZipcode.
 *
 * @see DealerZipcode
 */
@Service("monza.DealerZipcodeService")
public class DealerZipcodeServiceImpl implements DealerZipcodeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DealerZipcodeServiceImpl.class);


    @Autowired
    @Qualifier("monza.DealerZipcodeDao")
    private WMGenericDao<DealerZipcode, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<DealerZipcode, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "monzaTransactionManager")
    @Override
	public DealerZipcode create(DealerZipcode dealerZipcode) {
        LOGGER.debug("Creating a new DealerZipcode with information: {}", dealerZipcode);
        DealerZipcode dealerZipcodeCreated = this.wmGenericDao.create(dealerZipcode);
        return dealerZipcodeCreated;
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public DealerZipcode getById(Integer dealerzipcodeId) throws EntityNotFoundException {
        LOGGER.debug("Finding DealerZipcode by id: {}", dealerzipcodeId);
        DealerZipcode dealerZipcode = this.wmGenericDao.findById(dealerzipcodeId);
        if (dealerZipcode == null){
            LOGGER.debug("No DealerZipcode found with id: {}", dealerzipcodeId);
            throw new EntityNotFoundException(String.valueOf(dealerzipcodeId));
        }
        return dealerZipcode;
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public DealerZipcode findById(Integer dealerzipcodeId) {
        LOGGER.debug("Finding DealerZipcode by id: {}", dealerzipcodeId);
        return this.wmGenericDao.findById(dealerzipcodeId);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public DealerZipcode getByDealerIdAndZipcodeAndMakeAndModel(int dealerId, String zipcode, String make, String model) {
        Map<String, Object> dealerIdAndZipcodeAndMakeAndModelMap = new HashMap<>();
        dealerIdAndZipcodeAndMakeAndModelMap.put("dealerId", dealerId);
        dealerIdAndZipcodeAndMakeAndModelMap.put("zipcode", zipcode);
        dealerIdAndZipcodeAndMakeAndModelMap.put("make", make);
        dealerIdAndZipcodeAndMakeAndModelMap.put("model", model);

        LOGGER.debug("Finding DealerZipcode by unique keys: {}", dealerIdAndZipcodeAndMakeAndModelMap);
        DealerZipcode dealerZipcode = this.wmGenericDao.findByUniqueKey(dealerIdAndZipcodeAndMakeAndModelMap);

        if (dealerZipcode == null){
            LOGGER.debug("No DealerZipcode found with given unique key values: {}", dealerIdAndZipcodeAndMakeAndModelMap);
            throw new EntityNotFoundException(String.valueOf(dealerIdAndZipcodeAndMakeAndModelMap));
        }

        return dealerZipcode;
    }

	@Transactional(rollbackFor = EntityNotFoundException.class, value = "monzaTransactionManager")
	@Override
	public DealerZipcode update(DealerZipcode dealerZipcode) throws EntityNotFoundException {
        LOGGER.debug("Updating DealerZipcode with information: {}", dealerZipcode);
        this.wmGenericDao.update(dealerZipcode);

        Integer dealerzipcodeId = dealerZipcode.getDealerZipcodeId();

        return this.wmGenericDao.findById(dealerzipcodeId);
    }

    @Transactional(value = "monzaTransactionManager")
	@Override
	public DealerZipcode delete(Integer dealerzipcodeId) throws EntityNotFoundException {
        LOGGER.debug("Deleting DealerZipcode with id: {}", dealerzipcodeId);
        DealerZipcode deleted = this.wmGenericDao.findById(dealerzipcodeId);
        if (deleted == null) {
            LOGGER.debug("No DealerZipcode found with id: {}", dealerzipcodeId);
            throw new EntityNotFoundException(String.valueOf(dealerzipcodeId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public Page<DealerZipcode> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all DealerZipcodes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Page<DealerZipcode> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all DealerZipcodes");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service monza for table DealerZipcode to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "monzaTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

