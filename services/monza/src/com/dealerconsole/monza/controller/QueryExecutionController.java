/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/

package com.dealerconsole.monza.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import com.dealerconsole.monza.service.MonzaQueryExecutorService;
import com.dealerconsole.monza.models.query.*;

@RestController(value = "Monza.QueryExecutionController")
@RequestMapping("/monza/queryExecutor")
@Api(value = "QueryExecutionController", description = "controller class for query execution")
public class QueryExecutionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryExecutionController.class);

    @Autowired
    private MonzaQueryExecutorService queryService;

    @RequestMapping(value = "/queries/UsersByDealerId", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "No description is provided")
    public Page<UsersByDealerIdResponse> executeUsersByDealerId(@RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Executing named query: UsersByDealerId");
        Page<UsersByDealerIdResponse> _result = queryService.executeUsersByDealerId(dealerId, pageable);
        LOGGER.debug("got the result for named query: UsersByDealerId, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query UsersByDealerId")
    @RequestMapping(value = "/queries/UsersByDealerId/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportUsersByDealerId(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Exporting named query: UsersByDealerId");

        return queryService.exportUsersByDealerId(exportType, dealerId, pageable);
    }

    @RequestMapping(value = "/queries/SelectedUser", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Logged In user")
    public Page<SelectedUserResponse> executeSelectedUser(@RequestParam(value = "username", required = false) String username, @RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Executing named query: SelectedUser");
        Page<SelectedUserResponse> _result = queryService.executeSelectedUser(username, dealerId, pageable);
        LOGGER.debug("got the result for named query: SelectedUser, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query SelectedUser")
    @RequestMapping(value = "/queries/SelectedUser/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportSelectedUser(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "username", required = false) String username, @RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Exporting named query: SelectedUser");

        return queryService.exportSelectedUser(exportType, username, dealerId, pageable);
    }

    @RequestMapping(value = "/queries/DealerDiscounts", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "No description is provided")
    public Page<DealerDiscountsResponse> executeDealerDiscounts(@RequestParam(value = "dealerId", required = false) Integer dealerId, @RequestParam(value = "year", required = false) Integer year, @RequestParam(value = "make", required = false) String make, @RequestParam(value = "model", required = false) String model, @RequestParam(value = "trim", required = false) String trim, Pageable pageable) {
        LOGGER.debug("Executing named query: DealerDiscounts");
        Page<DealerDiscountsResponse> _result = queryService.executeDealerDiscounts(dealerId, year, make, model, trim, pageable);
        LOGGER.debug("got the result for named query: DealerDiscounts, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query DealerDiscounts")
    @RequestMapping(value = "/queries/DealerDiscounts/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportDealerDiscounts(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "dealerId", required = false) Integer dealerId, @RequestParam(value = "year", required = false) Integer year, @RequestParam(value = "make", required = false) String make, @RequestParam(value = "model", required = false) String model, @RequestParam(value = "trim", required = false) String trim, Pageable pageable) {
        LOGGER.debug("Exporting named query: DealerDiscounts");

        return queryService.exportDealerDiscounts(exportType, dealerId, year, make, model, trim, pageable);
    }

    @RequestMapping(value = "/queries/PriceByVin", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "No description is provided")
    public Page<PriceByVinResponse> executePriceByVin(@RequestParam(value = "vin", required = false) String vin, @RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Executing named query: PriceByVin");
        Page<PriceByVinResponse> _result = queryService.executePriceByVin(vin, dealerId, pageable);
        LOGGER.debug("got the result for named query: PriceByVin, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query PriceByVin")
    @RequestMapping(value = "/queries/PriceByVin/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportPriceByVin(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "vin", required = false) String vin, @RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Exporting named query: PriceByVin");

        return queryService.exportPriceByVin(exportType, vin, dealerId, pageable);
    }

    @RequestMapping(value = "/queries/PriceDiscounts", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "No description is provided")
    public Page<PriceDiscountsResponse> executePriceDiscounts(@RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Executing named query: PriceDiscounts");
        Page<PriceDiscountsResponse> _result = queryService.executePriceDiscounts(dealerId, pageable);
        LOGGER.debug("got the result for named query: PriceDiscounts, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query PriceDiscounts")
    @RequestMapping(value = "/queries/PriceDiscounts/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportPriceDiscounts(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "dealerId", required = false) Integer dealerId, Pageable pageable) {
        LOGGER.debug("Exporting named query: PriceDiscounts");

        return queryService.exportPriceDiscounts(exportType, dealerId, pageable);
    }

    @RequestMapping(value = "/queries/DealerRebates", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "No description is provided")
    public Page<DealerRebatesResponse> executeDealerRebates(@RequestParam(value = "dealerId", required = false) Integer dealerId, @RequestParam(value = "year", required = false) Integer year, @RequestParam(value = "make", required = false) String make, @RequestParam(value = "model", required = false) String model, @RequestParam(value = "trim", required = false) String trim, Pageable pageable) {
        LOGGER.debug("Executing named query: DealerRebates");
        Page<DealerRebatesResponse> _result = queryService.executeDealerRebates(dealerId, year, make, model, trim, pageable);
        LOGGER.debug("got the result for named query: DealerRebates, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query DealerRebates")
    @RequestMapping(value = "/queries/DealerRebates/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportDealerRebates(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "dealerId", required = false) Integer dealerId, @RequestParam(value = "year", required = false) Integer year, @RequestParam(value = "make", required = false) String make, @RequestParam(value = "model", required = false) String model, @RequestParam(value = "trim", required = false) String trim, Pageable pageable) {
        LOGGER.debug("Exporting named query: DealerRebates");

        return queryService.exportDealerRebates(exportType, dealerId, year, make, model, trim, pageable);
    }

    @RequestMapping(value = "/queries/DealerInfo", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "No description is provided")
    public Page<DealerInfoResponse> executeDealerInfo(@RequestParam(value = "id", required = false) Integer id, Pageable pageable) {
        LOGGER.debug("Executing named query: DealerInfo");
        Page<DealerInfoResponse> _result = queryService.executeDealerInfo(id, pageable);
        LOGGER.debug("got the result for named query: DealerInfo, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query DealerInfo")
    @RequestMapping(value = "/queries/DealerInfo/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportDealerInfo(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "id", required = false) Integer id, Pageable pageable) {
        LOGGER.debug("Exporting named query: DealerInfo");

        return queryService.exportDealerInfo(exportType, id, pageable);
    }

    @RequestMapping(value = "/queries/GetUserByDealers", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Retrieve user information")
    public Page<GetUserByDealersResponse> executeGetUserByDealers(@RequestParam(value = "dealers", required = false) List<Integer> dealers, @RequestParam(value = "username", required = false) String username, Pageable pageable) {
        LOGGER.debug("Executing named query: GetUserByDealers");
        Page<GetUserByDealersResponse> _result = queryService.executeGetUserByDealers(dealers, username, pageable);
        LOGGER.debug("got the result for named query: GetUserByDealers, result:{}", _result);
        return _result;
    }

    @ApiOperation(value = "Returns downloadable file for query GetUserByDealers")
    @RequestMapping(value = "/queries/GetUserByDealers/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportGetUserByDealers(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "dealers", required = false) List<Integer> dealers, @RequestParam(value = "username", required = false) String username, Pageable pageable) {
        LOGGER.debug("Exporting named query: GetUserByDealers");

        return queryService.exportGetUserByDealers(exportType, dealers, username, pageable);
    }

}


