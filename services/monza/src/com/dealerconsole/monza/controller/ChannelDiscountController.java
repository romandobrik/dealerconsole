/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.TypeMismatchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.DownloadResponse;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.dealerconsole.monza.ChannelDiscount;
import com.dealerconsole.monza.service.ChannelDiscountService;


/**
 * Controller object for domain model class ChannelDiscount.
 * @see ChannelDiscount
 */
@RestController("monza.ChannelDiscountController")
@Api(value = "ChannelDiscountController", description = "Exposes APIs to work with ChannelDiscount resource.")
@RequestMapping("/monza/ChannelDiscount")
public class ChannelDiscountController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelDiscountController.class);

    @Autowired
	@Qualifier("monza.ChannelDiscountService")
	private ChannelDiscountService channelDiscountService;

	@ApiOperation(value = "Creates a new ChannelDiscount instance.")
	@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public ChannelDiscount createChannelDiscount(@RequestBody ChannelDiscount channelDiscount) {
		LOGGER.debug("Create ChannelDiscount with information: {}" , channelDiscount);

		channelDiscount = channelDiscountService.create(channelDiscount);
		LOGGER.debug("Created ChannelDiscount with information: {}" , channelDiscount);

	    return channelDiscount;
	}

	@ApiOperation(value = "Creates a new ChannelDiscount instance.This API should be used when the ChannelDiscount instance has fields that requires multipart data.")
	@RequestMapping(method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ChannelDiscount createChannelDiscount(MultipartHttpServletRequest multipartHttpServletRequest) {
    	ChannelDiscount channelDiscount = WMMultipartUtils.toObject(multipartHttpServletRequest, ChannelDiscount.class, "monza"); 
        LOGGER.debug("Creating a new ChannelDiscount with information: {}" , channelDiscount);
        return channelDiscountService.create(channelDiscount);
    }


    @ApiOperation(value = "Returns the ChannelDiscount instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ChannelDiscount getChannelDiscount(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting ChannelDiscount with id: {}" , id);

        ChannelDiscount foundChannelDiscount = channelDiscountService.getById(id);
        LOGGER.debug("ChannelDiscount details with id: {}" , foundChannelDiscount);

        return foundChannelDiscount;
    }

    @ApiOperation(value = "Retrieves content for the given BLOB field in ChannelDiscount instance" )
    @RequestMapping(value = "/{id}/content/{fieldName}", method = RequestMethod.GET, produces="application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public DownloadResponse getChannelDiscountBLOBContent(@PathVariable("id") Integer id, @PathVariable("fieldName") String fieldName, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @RequestParam(value="download", defaultValue = "false") boolean download) {

        LOGGER.debug("Retrieves content for the given BLOB field {} in ChannelDiscount instance" , fieldName);

        if(!WMRuntimeUtils.isLob(ChannelDiscount.class, fieldName)) {
            throw new TypeMismatchException("Given field " + fieldName + " is not a valid BLOB type");
        }
        ChannelDiscount channelDiscount = channelDiscountService.getById(id);

        return WMMultipartUtils.buildDownloadResponseForBlob(channelDiscount, fieldName, httpServletRequest, download);
    }

    @ApiOperation(value = "Updates the ChannelDiscount instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ChannelDiscount editChannelDiscount(@PathVariable("id") Integer id, @RequestBody ChannelDiscount channelDiscount) throws EntityNotFoundException {
        LOGGER.debug("Editing ChannelDiscount with id: {}" , channelDiscount.getChannelDiscountId());

        channelDiscount.setChannelDiscountId(id);
        channelDiscount = channelDiscountService.update(channelDiscount);
        LOGGER.debug("ChannelDiscount details with id: {}" , channelDiscount);

        return channelDiscount;
    }

    @ApiOperation(value = "Updates the ChannelDiscount instance associated with the given id.This API should be used when ChannelDiscount instance fields that require multipart data.") 
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public ChannelDiscount editChannelDiscount(@PathVariable("id") Integer id, MultipartHttpServletRequest multipartHttpServletRequest) throws EntityNotFoundException {
        ChannelDiscount newChannelDiscount = WMMultipartUtils.toObject(multipartHttpServletRequest, ChannelDiscount.class, "monza");
        newChannelDiscount.setChannelDiscountId(id);

        ChannelDiscount oldChannelDiscount = channelDiscountService.getById(id);
        WMMultipartUtils.updateLobsContent(oldChannelDiscount, newChannelDiscount);
        LOGGER.debug("Updating ChannelDiscount with information: {}" , newChannelDiscount);

        return channelDiscountService.update(newChannelDiscount);
    }

    @ApiOperation(value = "Deletes the ChannelDiscount instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteChannelDiscount(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting ChannelDiscount with id: {}" , id);

        ChannelDiscount deletedChannelDiscount = channelDiscountService.delete(id);

        return deletedChannelDiscount != null;
    }

    /**
     * @deprecated Use {@link #findChannelDiscounts(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of ChannelDiscount instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ChannelDiscount> searchChannelDiscountsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering ChannelDiscounts list");
        return channelDiscountService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of ChannelDiscount instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ChannelDiscount> findChannelDiscounts(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering ChannelDiscounts list");
        return channelDiscountService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of ChannelDiscount instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<ChannelDiscount> filterChannelDiscounts(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering ChannelDiscounts list");
        return channelDiscountService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportChannelDiscounts(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return channelDiscountService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of ChannelDiscount instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countChannelDiscounts( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting ChannelDiscounts");
		return channelDiscountService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getChannelDiscountAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return channelDiscountService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service ChannelDiscountService instance
	 */
	protected void setChannelDiscountService(ChannelDiscountService service) {
		this.channelDiscountService = service;
	}

}

