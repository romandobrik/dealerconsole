/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.dealerconsole.monza.UsMarkets;
import com.dealerconsole.monza.service.UsMarketsService;


/**
 * Controller object for domain model class UsMarkets.
 * @see UsMarkets
 */
@RestController("monza.UsMarketsController")
@Api(value = "UsMarketsController", description = "Exposes APIs to work with UsMarkets resource.")
@RequestMapping("/monza/UsMarkets")
public class UsMarketsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsMarketsController.class);

    @Autowired
	@Qualifier("monza.UsMarketsService")
	private UsMarketsService usMarketsService;

	@ApiOperation(value = "Creates a new UsMarkets instance.")
	@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public UsMarkets createUsMarkets(@RequestBody UsMarkets usMarkets) {
		LOGGER.debug("Create UsMarkets with information: {}" , usMarkets);

		usMarkets = usMarketsService.create(usMarkets);
		LOGGER.debug("Created UsMarkets with information: {}" , usMarkets);

	    return usMarkets;
	}


    @ApiOperation(value = "Returns the UsMarkets instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public UsMarkets getUsMarkets(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting UsMarkets with id: {}" , id);

        UsMarkets foundUsMarkets = usMarketsService.getById(id);
        LOGGER.debug("UsMarkets details with id: {}" , foundUsMarkets);

        return foundUsMarkets;
    }

    @ApiOperation(value = "Updates the UsMarkets instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public UsMarkets editUsMarkets(@PathVariable("id") Integer id, @RequestBody UsMarkets usMarkets) throws EntityNotFoundException {
        LOGGER.debug("Editing UsMarkets with id: {}" , usMarkets.getMarketCode());

        usMarkets.setMarketCode(id);
        usMarkets = usMarketsService.update(usMarkets);
        LOGGER.debug("UsMarkets details with id: {}" , usMarkets);

        return usMarkets;
    }

    @ApiOperation(value = "Deletes the UsMarkets instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteUsMarkets(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting UsMarkets with id: {}" , id);

        UsMarkets deletedUsMarkets = usMarketsService.delete(id);

        return deletedUsMarkets != null;
    }

    /**
     * @deprecated Use {@link #findUsMarkets(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of UsMarkets instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<UsMarkets> searchUsMarketsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering UsMarkets list");
        return usMarketsService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of UsMarkets instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<UsMarkets> findUsMarkets(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering UsMarkets list");
        return usMarketsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of UsMarkets instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<UsMarkets> filterUsMarkets(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering UsMarkets list");
        return usMarketsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportUsMarkets(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return usMarketsService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of UsMarkets instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countUsMarkets( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting UsMarkets");
		return usMarketsService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getUsMarketsAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return usMarketsService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service UsMarketsService instance
	 */
	protected void setUsMarketsService(UsMarketsService service) {
		this.usMarketsService = service;
	}

}

