/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.dealerconsole.monza.Tbl02Models;
import com.dealerconsole.monza.service.Tbl02ModelsService;


/**
 * Controller object for domain model class Tbl02Models.
 * @see Tbl02Models
 */
@RestController("monza.Tbl02ModelsController")
@Api(value = "Tbl02ModelsController", description = "Exposes APIs to work with Tbl02Models resource.")
@RequestMapping("/monza/Tbl02Models")
public class Tbl02ModelsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(Tbl02ModelsController.class);

    @Autowired
	@Qualifier("monza.Tbl02ModelsService")
	private Tbl02ModelsService tbl02ModelsService;

	@ApiOperation(value = "Creates a new Tbl02Models instance.")
	@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Tbl02Models createTbl02Models(@RequestBody Tbl02Models tbl02models) {
		LOGGER.debug("Create Tbl02Models with information: {}" , tbl02models);

		tbl02models = tbl02ModelsService.create(tbl02models);
		LOGGER.debug("Created Tbl02Models with information: {}" , tbl02models);

	    return tbl02models;
	}


    @ApiOperation(value = "Returns the Tbl02Models instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Tbl02Models getTbl02Models(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Tbl02Models with id: {}" , id);

        Tbl02Models foundTbl02Models = tbl02ModelsService.getById(id);
        LOGGER.debug("Tbl02Models details with id: {}" , foundTbl02Models);

        return foundTbl02Models;
    }

    @ApiOperation(value = "Updates the Tbl02Models instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Tbl02Models editTbl02Models(@PathVariable("id") Integer id, @RequestBody Tbl02Models tbl02models) throws EntityNotFoundException {
        LOGGER.debug("Editing Tbl02Models with id: {}" , tbl02models.getModelId());

        tbl02models.setModelId(id);
        tbl02models = tbl02ModelsService.update(tbl02models);
        LOGGER.debug("Tbl02Models details with id: {}" , tbl02models);

        return tbl02models;
    }

    @ApiOperation(value = "Deletes the Tbl02Models instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteTbl02Models(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Tbl02Models with id: {}" , id);

        Tbl02Models deletedTbl02Models = tbl02ModelsService.delete(id);

        return deletedTbl02Models != null;
    }

    /**
     * @deprecated Use {@link #findTbl02Models(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Tbl02Models instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Tbl02Models> searchTbl02ModelsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Tbl02Models list");
        return tbl02ModelsService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Tbl02Models instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Tbl02Models> findTbl02Models(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Tbl02Models list");
        return tbl02ModelsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Tbl02Models instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Tbl02Models> filterTbl02Models(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Tbl02Models list");
        return tbl02ModelsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportTbl02Models(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return tbl02ModelsService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Tbl02Models instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countTbl02Models( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Tbl02Models");
		return tbl02ModelsService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getTbl02ModelsAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return tbl02ModelsService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service Tbl02ModelsService instance
	 */
	protected void setTbl02ModelsService(Tbl02ModelsService service) {
		this.tbl02ModelsService = service;
	}

}

