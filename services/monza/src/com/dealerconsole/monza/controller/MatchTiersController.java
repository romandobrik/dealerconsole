/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.TypeMismatchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.DownloadResponse;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.dealerconsole.monza.MatchTiers;
import com.dealerconsole.monza.service.MatchTiersService;


/**
 * Controller object for domain model class MatchTiers.
 * @see MatchTiers
 */
@RestController("monza.MatchTiersController")
@Api(value = "MatchTiersController", description = "Exposes APIs to work with MatchTiers resource.")
@RequestMapping("/monza/MatchTiers")
public class MatchTiersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchTiersController.class);

    @Autowired
	@Qualifier("monza.MatchTiersService")
	private MatchTiersService matchTiersService;

	@ApiOperation(value = "Creates a new MatchTiers instance.")
	@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public MatchTiers createMatchTiers(@RequestBody MatchTiers matchTiers) {
		LOGGER.debug("Create MatchTiers with information: {}" , matchTiers);

		matchTiers = matchTiersService.create(matchTiers);
		LOGGER.debug("Created MatchTiers with information: {}" , matchTiers);

	    return matchTiers;
	}

	@ApiOperation(value = "Creates a new MatchTiers instance.This API should be used when the MatchTiers instance has fields that requires multipart data.")
	@RequestMapping(method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public MatchTiers createMatchTiers(MultipartHttpServletRequest multipartHttpServletRequest) {
    	MatchTiers matchTiers = WMMultipartUtils.toObject(multipartHttpServletRequest, MatchTiers.class, "monza"); 
        LOGGER.debug("Creating a new MatchTiers with information: {}" , matchTiers);
        return matchTiersService.create(matchTiers);
    }


    @ApiOperation(value = "Returns the MatchTiers instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public MatchTiers getMatchTiers(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting MatchTiers with id: {}" , id);

        MatchTiers foundMatchTiers = matchTiersService.getById(id);
        LOGGER.debug("MatchTiers details with id: {}" , foundMatchTiers);

        return foundMatchTiers;
    }

    @ApiOperation(value = "Retrieves content for the given BLOB field in MatchTiers instance" )
    @RequestMapping(value = "/{id}/content/{fieldName}", method = RequestMethod.GET, produces="application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public DownloadResponse getMatchTiersBLOBContent(@PathVariable("id") Integer id, @PathVariable("fieldName") String fieldName, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @RequestParam(value="download", defaultValue = "false") boolean download) {

        LOGGER.debug("Retrieves content for the given BLOB field {} in MatchTiers instance" , fieldName);

        if(!WMRuntimeUtils.isLob(MatchTiers.class, fieldName)) {
            throw new TypeMismatchException("Given field " + fieldName + " is not a valid BLOB type");
        }
        MatchTiers matchTiers = matchTiersService.getById(id);

        return WMMultipartUtils.buildDownloadResponseForBlob(matchTiers, fieldName, httpServletRequest, download);
    }

    @ApiOperation(value = "Updates the MatchTiers instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public MatchTiers editMatchTiers(@PathVariable("id") Integer id, @RequestBody MatchTiers matchTiers) throws EntityNotFoundException {
        LOGGER.debug("Editing MatchTiers with id: {}" , matchTiers.getMatchTierId());

        matchTiers.setMatchTierId(id);
        matchTiers = matchTiersService.update(matchTiers);
        LOGGER.debug("MatchTiers details with id: {}" , matchTiers);

        return matchTiers;
    }

    @ApiOperation(value = "Updates the MatchTiers instance associated with the given id.This API should be used when MatchTiers instance fields that require multipart data.") 
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public MatchTiers editMatchTiers(@PathVariable("id") Integer id, MultipartHttpServletRequest multipartHttpServletRequest) throws EntityNotFoundException {
        MatchTiers newMatchTiers = WMMultipartUtils.toObject(multipartHttpServletRequest, MatchTiers.class, "monza");
        newMatchTiers.setMatchTierId(id);

        MatchTiers oldMatchTiers = matchTiersService.getById(id);
        WMMultipartUtils.updateLobsContent(oldMatchTiers, newMatchTiers);
        LOGGER.debug("Updating MatchTiers with information: {}" , newMatchTiers);

        return matchTiersService.update(newMatchTiers);
    }

    @ApiOperation(value = "Deletes the MatchTiers instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteMatchTiers(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting MatchTiers with id: {}" , id);

        MatchTiers deletedMatchTiers = matchTiersService.delete(id);

        return deletedMatchTiers != null;
    }

    @RequestMapping(value = "/dealerId-tier-make-model", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the matching MatchTiers with given unique key values.")
    public MatchTiers getByDealerIdAndTierAndMakeAndModel(@RequestParam("dealerId") int dealerId, @RequestParam("tier") int tier, @RequestParam("make") String make, @RequestParam("model") String model) {
        LOGGER.debug("Getting MatchTiers with uniques key DealerIdAndTierAndMakeAndModel");
        return matchTiersService.getByDealerIdAndTierAndMakeAndModel(dealerId, tier, make, model);
    }

    /**
     * @deprecated Use {@link #findMatchTiers(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of MatchTiers instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<MatchTiers> searchMatchTiersByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering MatchTiers list");
        return matchTiersService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of MatchTiers instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<MatchTiers> findMatchTiers(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering MatchTiers list");
        return matchTiersService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of MatchTiers instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<MatchTiers> filterMatchTiers(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering MatchTiers list");
        return matchTiersService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportMatchTiers(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return matchTiersService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of MatchTiers instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countMatchTiers( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting MatchTiers");
		return matchTiersService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getMatchTiersAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return matchTiersService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service MatchTiersService instance
	 */
	protected void setMatchTiersService(MatchTiersService service) {
		this.matchTiersService = service;
	}

}

