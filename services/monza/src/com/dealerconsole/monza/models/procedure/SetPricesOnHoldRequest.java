/*Copyright (c) 2015-2016 uquote.io All Rights Reserved.
 This software is the confidential and proprietary information of uquote.io You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with uquote.io*/
package com.dealerconsole.monza.models.procedure;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SetPricesOnHoldRequest implements Serializable {

    @JsonProperty("modifiedby")
    private String modifiedby;
    @JsonProperty("dealerId")
    private Integer dealerId;
    @JsonProperty("channel")
    private Integer channel;

    public String getModifiedby() {
        return this.modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Integer getDealerId() {
        return this.dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getChannel() {
        return this.channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SetPricesOnHoldRequest)) return false;
        final SetPricesOnHoldRequest setPricesOnHoldRequest = (SetPricesOnHoldRequest) o;
        return Objects.equals(getModifiedby(), setPricesOnHoldRequest.getModifiedby()) &&
                Objects.equals(getDealerId(), setPricesOnHoldRequest.getDealerId()) &&
                Objects.equals(getChannel(), setPricesOnHoldRequest.getChannel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getModifiedby(),
                getDealerId(),
                getChannel());
    }
}
