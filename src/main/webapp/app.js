Application.run(function($rootScope) {
    "use strict";
    /* perform any action on the variables within this block(on-page-load) */
    $rootScope.onAppVariablesReady = function() {
        /*
         * variables can be accessed through '$rootScope.Variables' property here
         * e.g. $rootScope.Variables.staticVariable1.getData()
         */

    };

    $rootScope.VehicleDescriptionInvokeonSuccess = function(variable, data) {
        console.group("VehicleDescriptionInvokeonSuccess");
        console.log(data);
        var features = data.features;

        var featuresTree = [];

        var featureItem = {
            "label": "Exterior",
            "children": []
        };

        featureItem.children = setChildren(features.EXTERIOR);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Entertainment",
            "children": []
        };

        featureItem.children = setChildren(features.ENTERTAINMENT);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Interior",
            "children": []
        };

        featureItem.children = setChildren(features.INTERIOR);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Safety",
            "children": []
        };

        featureItem.children = setChildren(features.SAFETY);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Mechanical",
            "children": []
        };

        featureItem.children = setChildren(features.MECHANICAL);

        featuresTree.push(featureItem);

        $rootScope.Variables.featuresVariable.dataSet = featuresTree;

        console.log($rootScope.Variables.featuresVariable);

        $rootScope.Variables.optionsVariable.dataSet = setOptions(data.options);

        console.log($rootScope.Variables.optionsVariable);

        console.groupEnd();
    };


    function setChildren(feature) {
        var children = [];
        console.log(feature);
        for (var i = 0; i < feature.length; i++) {
            var item = {
                "label": null
            };
            item.label = feature[i];
            children.push(item);
        }

        return children;
    }


    function setOptions(options) {

        var optionsTree = [];


        for (var j = 0; j < options.length; j++) {
            var item = {
                "Option": null,
                "Price": 0,
                "children": []
            };
            item.Option = options[j].name;
            item.Price = options[j].price.baseMSRP;

            var children = [];

            if (options[j].attributes) {
                for (var k = 0; k < options[j].attributes.length; k++) {
                    var attribute = {
                        "Option": null
                    };
                    attribute.Option = options[j].attributes[k].name;
                    children.push(attribute);
                }
            }

            item.children = children;
            optionsTree.push(item);
        }

        return optionsTree;
    }


    $rootScope.VehicleDiffInvokeonSuccess = function(variable, data) {
        console.group("VehicleDiffInvokeonSuccess");
        console.log(data);
        $rootScope.Variables.vehicle1OptionsVariable.dataSet = setOptions(data.vehicle1.options);
        console.log($rootScope.Variables.vehicle1OptionsVariable);
        $rootScope.Variables.vehicle2OptionsVariable.dataSet = setOptions(data.vehicle2.options);
        console.log($rootScope.Variables.vehicle2OptionsVariable);
        console.groupEnd();
    };



    $rootScope.MonzaPriceSetupDataonBeforeUpdate = function(variable, data) {
        console.group("MonzaPriceSetupDataonBeforeUpdate");
        console.log(data);
        console.log($rootScope.Variables.MonzaPriceSetupData);
        console.groupEnd();
    };


    $rootScope.MonzaDealerZipcodeDataonBeforeUpdate = function(variable, data) {
        console.group("MonzaDealerZipcodeDataonBeforeUpdate");
        console.log(data);
        console.log($rootScope.Variables.MonzaDealerZipcodeData);
        console.groupEnd();
    };


    $rootScope.VehicleDescriptionInvokeonResult = function(variable, data) {
        console.group("VehicleDescriptionInvokeonResult");
        console.log(data);
        console.groupEnd();
    };


    $rootScope.selectedUseronSuccess = function(variable, data) {
        console.group("selectedUseronSuccess");
        console.log(data);

        var roles = [];

        var i = $rootScope.Variables.loggedInUser.dataSet.roles.length;
        while (i--) {
            if (!isNaN($rootScope.Variables.loggedInUser.dataSet.roles[i])) {
                roles.push($rootScope.Variables.loggedInUser.dataSet.roles[i]);
            }
        }

        //roles.push(data[0].role);

        //$rootScope.Variables.loggedInUser.dataSet.roles = roles;

        console.log($rootScope.Variables.loggedInUser.dataSet.roles);

        console.groupEnd();
    };

});