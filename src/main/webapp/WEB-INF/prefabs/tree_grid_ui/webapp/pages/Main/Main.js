Application.$controller("Tree_grid_uiController", ["$scope", function($scope) {
    "use strict";
    console.log("Init Prefab");
    $scope.tree_data = [];

    $scope.col_defs = [{
        "field": "Price",
        "sortable": true,
        "sortingType": "number",
        "filterable": true
    }];

    /* 
     * This function will be invoked when any of this prefab's property is changed
     * @key: property name
     * @newVal: new value of the property
     * @oldVal: old value of the property
     */
    function propertyChangeHandler(key, newVal, oldVal) {

            switch (key) {
                case "list":
                    console.log(newVal);
                    $scope.tree_data = newVal;
                    break;
                case "cols":
                    console.log(newVal);
                    $scope.col_defs = newVal;
                    break;
            }

        }
        /* register the property change handler */
    $scope.propertyManager.add($scope.propertyManager.ACTIONS.CHANGE, propertyChangeHandler);

    $scope.onInitPrefab = function() {

        // this method will be triggered post initialization of the prefab.
    }

}]);