Application.$controller("PubnubPrefabController", ["$scope", "$rootScope", "Pubnub", function($scope, $rootScope, Pubnub) {
    "use strict";
    console.log("PubNub Init");
    Pubnub.init({
        //publish_key: 'demo',
        //subscribe_key: 'demo'
        publish_key: $scope.publishkey,
        subscribe_key: $scope.subscribekey
    });
    /* 
     * This function will be invoked when any of this prefab's property is changed
     * @key: property name
     * @newVal: new value of the property
     * @oldVal: old value of the property
     */
    function propertyChangeHandler(key, newVal, oldVal) {
            switch (key) {
                case "request":
                    console.log(newVal);
                    if (newVal)
                        Pubnub.publish({
                            channel: $scope.requestchannel,
                            message: newVal,
                            callback: function(info) {
                                console.log(info)
                            }
                        });
                    break;
                case "subscribechannel":
                    if (newVal) {
                        $scope.subscribechannel = newVal;
                        console.log("Subsribed to channel " + $scope.subscribechannel);
                        Pubnub.subscribe({
                            channel: $scope.subscribechannel,
                            message: $scope.newMessage,
                            triggerEvents: ['callback', 'presence']
                        });
                    }
                    break;
                case "requestchannel":
                    if (newVal)
                        $scope.requestchannel = newVal;
                    break;
                case "response":
                    // do something with newVal for property 'prop2'
                    break;
            }

        }
        /* register the property change handler */
    $scope.propertyManager.add($scope.propertyManager.ACTIONS.CHANGE, propertyChangeHandler);



    $rootScope.$on(Pubnub.getMessageEventNameFor($scope.subscribechannel), function(ngEvent, message, envelope, channel) {
        $scope.$apply(function() {
            $rootScope.$broadcast('PUBNUB_RECEIVED', {
                channel: $scope.subscribechannel,
                message: message
            });
            console.log(message);
        });
    });


    /*
        Pubnub.subscribe({
            channel: $scope.subscribechannel,
            message: function(message) {

                console.log(message);
                if (arguments) {
                    $scope.response = arguments[0];

                    $rootScope.$broadcast('PUBNUB_RECEIVED', {
                        channel: $scope.subscribechannel,
                        message: $scope.response
                    });
                    console.log($scope.response);

                }
            },
            error: function(error) {
                // Handle error here
                console.log("Error:" + JSON.stringify(error));
            }
        });
        */

    $scope.onInitPrefab = function() {

        Pubnub.time(
            function(time) {
                console.log(time)
            }
        );
    }

}]);