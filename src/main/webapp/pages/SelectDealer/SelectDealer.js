Application.$controller("SelectDealerPageController", ["$scope", "$routeParams", function($scope, $routeParams) {
    "use strict";
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
        console.group("Select Dealers page variables");
        console.log($scope.Variables.loggedInUser);
        console.log($routeParams);

        if (typeof $scope.Variables.dealerId.dataSet.dataValue !== 'undefined' || $scope.Variables.dealerId.dataSet.dataValue === null) {

            if (typeof $routeParams.dealerid !== 'undefined') {
                var dealer = $routeParams.dealerid;

                console.log(dealer);
                if (!isNaN(dealer)) {
                    if (contains($scope.Variables.loggedInUser.dataSet.roles, dealer)) {
                        $scope.Variables.dealerId.dataSet.dataValue = dealer;

                        $scope.Variables.goToPage_Main.navigate();
                    } else
                        $scope.Variables.goToPage_Login.navigate();

                } else
                    $scope.Variables.goToPage_Login.navigate();

            }

        }


        var i = $scope.Variables.loggedInUser.dataSet.roles.length;
        while (i--) {
            if (!isNaN($scope.Variables.loggedInUser.dataSet.roles[i])) {
                $scope.Variables.dealerIds.dataSet.push(parseInt($scope.Variables.loggedInUser.dataSet.roles[i]));
            }
        }

        console.log($scope.Variables.dealerIds);

        //$scope.Variables.MonzaExecuteGetUserByDealers.update();

        console.log($scope.Variables.MonzaExecuteGetUserByDealers);

        console.groupEnd();
    };
    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.addRoleServiceVariableonSuccess = function(variable, data) {
        console.group("Selected Role");
        console.log(data);
        console.log($scope.Variables.loggedInUser);
        console.groupEnd();
    };

}]);

function contains(a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}


Application.$controller("selectDealerGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.customRowAction = function($event, $rowData) {
            console.group("Selected Dealer");
            console.log($rowData);

            $scope.Variables.dealerId.dataSet.dataValue = $rowData.dealer_id;

            $scope.Variables.addRoleServiceVariable.setInput('role', $rowData.role);
            $scope.Variables.addRoleServiceVariable.update();

            console.groupEnd();
        };

    }
]);