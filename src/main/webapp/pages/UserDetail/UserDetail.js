Application.$controller("UserDetailPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        console.group("UserDetailPageReady");
        $scope.Variables.DealerUserQuoteRequestsInvoke.setInput('userid', $scope.$parent.Variables.selectedUser.dataSet.dataValue);
        $scope.Variables.DealerUserQuoteRequestsInvoke.update();
        $scope.Variables.DealerScansInvoke.setInput('userid', $scope.$parent.Variables.selectedUser.dataSet.dataValue);
        $scope.Variables.DealerScansInvoke.update();
        $scope.Variables.DealerQuoteResponsesInvoke.setInput('userid', $scope.$parent.Variables.selectedUser.dataSet.dataValue);
        $scope.Variables.DealerQuoteResponsesInvoke.update();

        console.log($scope.Variables.DealerUserQuoteRequestsInvoke);
        console.groupEnd();

    };

}]);


Application.$controller("userRequestsGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("userScansGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("userResponsesGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);