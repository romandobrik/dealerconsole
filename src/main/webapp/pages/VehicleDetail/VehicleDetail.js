Application.$controller("VehicleDetailPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        console.group("VehicleDetailonPageReady");

        $scope.Variables.VehicleDetailVariable.setInput('vin', $scope.$parent.Variables.selectedVin.dataSet.dataValue);
        $scope.Variables.VehicleDetailVariable.setInput('zipcode', $scope.$parent.Variables.selectedZipcode.dataSet.dataValue);
        $scope.Variables.VehicleDetailVariable.update();

        $scope.Variables.ConsumerRebatesVariable.setInput('vin', $scope.$parent.Variables.selectedVin.dataSet.dataValue);
        $scope.Variables.ConsumerRebatesVariable.setInput('zipcode', $scope.$parent.Variables.selectedZipcode.dataSet.dataValue);
        $scope.Variables.ConsumerRebatesVariable.update();
        console.groupEnd();
    };


    $scope.VehicleDetailVariableonSuccess = function(variable, data) {
        console.group("VehicleDetailVariableonSuccess");
        console.log(data);
        var features = data.features;

        var featuresTree = [];

        var featureItem = {
            "label": "Exterior",
            "children": []
        };

        featureItem.children = setChildren(features.EXTERIOR);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Entertainment",
            "children": []
        };

        featureItem.children = setChildren(features.ENTERTAINMENT);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Interior",
            "children": []
        };

        featureItem.children = setChildren(features.INTERIOR);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Safety",
            "children": []
        };

        featureItem.children = setChildren(features.SAFETY);

        featuresTree.push(featureItem);

        featureItem = {
            "label": "Mechanical",
            "children": []
        };

        featureItem.children = setChildren(features.MECHANICAL);

        featuresTree.push(featureItem);

        $scope.Variables.detailFeaturesVariable.dataSet = featuresTree;

        console.log($scope.Variables.detailFeaturesVariable);

        $scope.Variables.detailOptionsVariable.dataSet = setOptions(data.options);

        console.log($scope.Variables.detailOptionsVariable);

        console.groupEnd();
    };


    function setChildren(feature) {
        var children = [];
        console.log(feature);
        for (var i = 0; i < feature.length; i++) {
            var item = {
                "label": null
            };
            item.label = feature[i];
            children.push(item);
        }

        return children;
    };


    function setOptions(options) {

        var optionsTree = [];


        for (var j = 0; j < options.length; j++) {
            var item = {
                "Option": null,
                "Price": 0,
                "children": []
            };
            item.Option = options[j].name;
            item.Price = options[j].price.baseMSRP;

            var children = [];

            if (options[j].attributes) {
                for (var k = 0; k < options[j].attributes.length; k++) {
                    var attribute = {
                        "Option": null
                    };
                    attribute.Option = options[j].attributes[k].name;
                    children.push(attribute);
                }
            }

            item.children = children;
            optionsTree.push(item);
        }

        return optionsTree;
    };


    $scope.ConsumerRebatesVariableonSuccess = function(variable, data) {
        console.group("ConsumerRebatesVariableonSuccess");
        console.log(data);
        if (data.ConsumerIncentives.length > 0) {
            $scope.Variables.detailRebatesVariable.dataSet = setSpecials(data.ConsumerIncentives[0].ConsumerSpecials);
            $scope.Widgets.treeGridRebates.show = true;
        } else
            $scope.Widgets.treeGridRebates.show = false;

        console.log($scope.Variables.detailRebatesVariable);

        console.groupEnd();
    };

    function setSpecials(specials) {

        var specialsTree = [];
        for (var j = 0; j < specials.length; j++) {
            var item = {
                "Type": null,
                "Price": 0
            };
            item.Type = specials[j].SpecialProgramName;
            item.Price = specials[j].Cash;

            specialsTree.push(item);
        }

        return specialsTree;
    };

}]);