Application.$controller("SendResponsePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

}]);


Application.$controller("modelGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.customRowAction = function($event, $rowData) {
            console.group("Send Make Response");

            $scope.Variables.sendModelResponseVariable.setInput('make', $rowData.make);
            $scope.Variables.sendModelResponseVariable.setInput('model', $rowData.model);
            $scope.Variables.sendModelResponseVariable.update();

            console.groupEnd();
        };

    }
]);

Application.$controller("trimGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.customRowAction = function($event, $rowData) {
            console.group("Send Trim Response");

            $scope.Variables.sendTrimResponseVariable.setInput('trim', $rowData.trim);
            $scope.Variables.sendTrimResponseVariable.update();

            console.groupEnd();
        };

    }
]);

Application.$controller("alertdialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("alertdialog2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("vehicleGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.customRowAction = function($event, $rowData) {
            console.group("Send Vehicle Response");

            $scope.Variables.sendVehicleResponseVariable.setInput('vin', $rowData.vin);
            $scope.Variables.sendVehicleResponseVariable.update();

            console.groupEnd();

        };

    }
]);