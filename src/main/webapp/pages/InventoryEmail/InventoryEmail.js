Application.$controller("InventoryEmailPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $scope.Variables.modelsEmailVariable.setInput('channel', $scope.$parent.Widgets.selectChannel.datavalue);

    };


    $scope.makeChange = function($event, $isolateScope) {
        console.group("EmailMakeChange");
        $scope.Variables.modelsEmailVariable.setInput('makeNiceName', $scope.Widgets.make.datavalue.toLowerCase());
        $scope.Variables.modelsEmailVariable.update();
        console.log($scope.Variables.modelsEmailVariable);
        console.groupEnd();
    };

}]);