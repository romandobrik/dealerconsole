Application.$controller("MainPageController", ["$scope", "$routeParams", function($scope, $routeParams) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
        console.group("Init Main page variables");
        console.log($scope.Variables.loggedInUser);
        console.log($routeParams);

        if (typeof $scope.Variables.dealerId.dataSet.dataValue !== 'undefined' || $scope.Variables.dealerId.dataSet.dataValue === null) {

            if (typeof $routeParams.dealerid !== 'undefined') {
                var dealer = $routeParams.dealerid;

                console.log(dealer);
                if (!isNaN(dealer)) {
                    if (contains($scope.Variables.loggedInUser.dataSet.roles, dealer)) {
                        $scope.Variables.dealerId.dataSet.dataValue = dealer;

                    } else
                        $scope.Variables.goToPage_Login.navigate();

                } else
                    $scope.Variables.goToPage_Login.navigate();

            }

        }
        $scope.Variables.subscribeChannelsVariable.dataSet.dataValue = "monza-dealer-" + $scope.Variables.dealerId.dataSet.dataValue;
        console.log($scope.Variables.subscribeChannelsVariable);

        console.groupEnd();
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */

        console.group("Init Main page");
        console.log($routeParams);


        //$routeParams.requestid = '9ACECDAF-5D26-44D0-9D42-54CB8F6C22DE';
        if (typeof $routeParams.requestid !== 'undefined') {
            $scope.Variables.requestIdVariable.dataSet.dataValue = $routeParams.requestid;
            $scope.Variables.DealerQuoteRequestByIDInvoke.update();
            $scope.Variables.MatchQuoteRequestInvoke.setInput('requestid', $routeParams.requestid);
            $scope.Variables.MatchQuoteRequestInvoke.update();
            console.log($scope.Variables.MatchQuoteRequestInvoke);
            $scope.Widgets.leadPanel.expanded = true;
            $scope.Widgets.requestPanel.expanded = false;
        } else if (typeof $scope.Widgets.requestPanel !== 'undefined') {
            $scope.Widgets.gridQuoteRequests.selectItem(0);
            //var dataSet = $scope.Variables.DealerQuoteRequestsInvoke.getData();
            //console.log(dataSet);
            //if (dataSet.requests.length > 0)
            //   rewrite(dataSet.requests[0]);

            $scope.Widgets.leadPanel.expanded = false;
            $scope.Widgets.requestPanel.expanded = true;
        } else {
            $scope.Widgets.gridScans.selectItem(0);
            $scope.Widgets.scanPanel.expanded = true;
        }

        console.log($scope.Widgets.leadPanel.expanded);
        console.groupEnd();

    };

    $scope.$on('PUBNUB_RECEIVED', function(event, data) {
        //alert(data.channel);
        if (data.message.event == 'CAR_QUOTE_REQUEST')
            $scope.Variables.DealerQuoteRequestsInvoke.update();
        //else if (data.message.event == 'CAR_SCAN')
        //    $scope.Variables.DealerScansInvoke.update();
        else if (data.message.event == 'CAR_QUOTE_CONFIRM') {
            $scope.Variables.DealerQuoteRequestsInvoke.update();
        } else if (data.message.event == 'CAR_QUOTE_RESPONSE_CONFIRM') {
            $scope.Variables.DealerQuoteRequestsInvoke.update();
        }

        console.log(data);
    });


    $scope.gridScansSelect = function($event, $data) {
        $scope.Widgets.usergridrow.show = false;
        $scope.Widgets.responsegridrow.show = false;
        rewrite($data);
    };


    $scope.gridQuoteRequestsSelect = function($event, $data) {
        $scope.Widgets.usergridrow.show = true;
        $scope.Widgets.responsegridrow.show = true;

        rewrite($data);

        $scope.Variables.DealerQuoteResponsesInvoke.setInput('requestid', $data.requestid);
        $scope.Variables.VehicleDescriptionInvoke.setInput('channel', $data.channel);

        $scope.Variables.DealerQuoteResponsesInvoke.update();

    };

    function rewrite($data) {
        console.group("Rewrite detail");
        console.log($data);

        $scope.Variables.selectedZipcode.dataSet.dataValue = $data.zipcode;

        $scope.Variables.VehicleDescriptionInvoke.setInput('vin', $data.vin);
        $scope.Variables.VehicleDescriptionInvoke.setInput('zipcode', $data.zipcode);

        $scope.Variables.VehicleDescriptionInvoke.update();

        $scope.Variables.imageUrlVariable.dataSet.url = $data.image;

        console.log($scope.Variables.imageUrlVariable);
        $scope.Variables.mapDataVariable.dataSet.lon = $data.longitude;
        $scope.Variables.mapDataVariable.dataSet.lat = $data.latitude;
        $scope.Variables.mapDataVariable.dataSet.info = $data.make + " " + $data.model + " " + $data.trim;

        $scope.Widgets.vehicle.caption = $data.year + " " + $data.make + " " + $data.model + " " + $data.trim;


        $scope.Widgets.popoverColor.caption = "Exterior:" + $data.color;
        $scope.Variables.colorVariable.dataSet.dataValue = "#" + $data.colorid;
        $scope.Variables.msrpVariable.dataSet.dataValue = $data.msrp;
        $scope.Widgets.popoverDealer.caption = $data.dealername;
        $scope.Widgets.labelVin.caption = "VIN:" + $data.vin;

        if ($data.channel === 0)
            $scope.Widgets.channelLabel.caption = "uQuote";
        else if ($data.channel === 1)
            $scope.Widgets.channelLabel.caption = "Direct";
        else if ($data.channel === 2)
            $scope.Widgets.channelLabel.caption = "Internet";
        else
            $scope.Widgets.channelLabel.caption = "";

        if ($data.make === "Ford" || $data.make === "Lincoln") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.inventory.ford.com/services/inventory/WindowSticker.pdf?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else if ($data.make === "Hyundai") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.stocktonhyundai.com/external-catalog-services/rest/monroney/windowsticker?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else if ($data.make === "Chrysler" || $data.make === "Dodge" || $data.make === "Ram" || $data.make === "Jeep") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.chrysler.com/hostd/windowsticker/getWindowStickerPdf.do?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else if ($data.make === "Infiniti") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.infinitiusa.com/buildyourinfiniti/inventory/windowSticker?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else
            $scope.Widgets.anchorSticker.show = false;



        if (typeof $data.optionalcolors !== 'undefined') {
            //if ($data.optionalcolors !== null) {
            $scope.Widgets.textareaOptionalColors.datavalue = $data.optionalcolors;
            $scope.Widgets.compositeOptionalColors.show = true;
        } else
            $scope.Widgets.compositeOptionalColors.show = false;

        if (typeof $data.finance !== 'undefined') {
            //if ($data.optionalcolors !== null) {
            $scope.Widgets.checkboxFinance.datavalue = $data.finance;
            $scope.Widgets.compositeFinance.show = true;
        } else
            $scope.Widgets.compositeFinance.show = false;

        if (typeof $data.tradein !== 'undefined') {
            //if ($data.optionalcolors !== null) {
            $scope.Widgets.checkboxTradeIn.datavalue = $data.tradein;
            $scope.Widgets.compositeTradeIn.show = true;
        } else
            $scope.Widgets.compositeTradeIn.show = false;

        console.log($scope.Widgets.anchorSticker.hyperlink);
        console.groupEnd();

    }

    function contains(a, obj) {
        var i = a.length;
        while (i--) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    }


    $scope.DealerQuoteRequestByIDInvokeonResult = function(variable, data) {
        if (typeof data.requests !== 'undefined') {
            if (data.requests.length > 0) {
                console.log(data.requests[0]);
                rewrite(data.requests[0]);
            }
        }
    };



    $scope.gridInventoryMatchSelect = function($event, $data) {
        console.log($data);
        $scope.Variables.VehicleDiffInvoke.setInput('vin2', $data.vin);
        $scope.Variables.VehicleDiffInvoke.setInput('zipcode2', $data.zipcode);
    };


    $scope.gridInventoryMatchRowdblclick = function($event, $data) {
        $scope.Variables.selectedVin.dataSet.dataValue = $data.vin;

        //$scope.Variables.selectedZipcode.dataSet.dataValue = Variables.MonzaDealersData.dataSet.data[0].zipcode;
    };


    $scope.gridQuoteRequestsRowdblclick = function($event, $data) {
        $scope.Variables.selectedVin.dataSet.dataValue = $data.vin;
        $scope.Variables.selectedZipcode.dataSet.dataValue = $data.zipcode;
    };


    $scope.gridScansRowdblclick = function($event, $data) {
        $scope.Variables.selectedVin.dataSet.dataValue = $data.vin;
        $scope.Variables.selectedZipcode.dataSet.dataValue = $data.zipcode;
    };



    $scope.gridResponsesRowdblclick = function($event, $data) {
        $scope.Variables.selectedVin.dataSet.dataValue = $data.vin;
        $scope.Variables.selectedZipcode.dataSet.dataValue = $data.zipcode;
    };


    $scope.DealerUserProfileInvokeonSuccess = function(variable, data) {
        console.group("DealerUserProfileInvokeonSuccess");
        console.log(data);

        if (typeof data !== 'undefined' && typeof data.userid !== 'undefined')
            $scope.Variables.selectedUser.dataSet.dataValue = data.userid.toUpperCase();

        if (typeof data !== 'undefined' && typeof data.profile_pic !== 'undefined') {
            $scope.Widgets.messageAnchor.show = true;

        } else {
            $scope.Widgets.messageAnchor.show = true;
        }

        console.groupEnd();
    };


    $scope.gridUserSelect = function($event, $rowData) {

    };


    $scope.userImageClick = function($event, $isolateScope) {
        var win = window.open($scope.Variables.fbMessengerUrlVariable.dataSet.dataValue, '_blank');
        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');
        }
    };


    $scope.VehicleDescriptionInvokeonSuccess = function(variable, data) {
        $scope.Variables.VehiclePriceVariable.dataSet.dataValue = data.price;
        $scope.Variables.VehicleEngineVariable.dataSet.dataValue = data.engine;
        $scope.Variables.VehicleDealerVariable.dataSet.dataValue = data.dealer;
        $scope.Variables.VehicleImagesVariable.dataSet.dataValue = data.imagelist;
        $scope.Variables.VehicleInteriorVariable.dataSet.dataValue = data.interiorColor;

    };

}]);


Application.$controller("gridScansController", ["$scope",
    function($scope) {
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("gridQuoteRequestsController", ["$scope",
    function($scope) {
        $scope.ctrlScope = $scope;

        $scope.customRowAction = function($event, rowData) {

        };

    }
]);

Application.$controller("gridInventoryMatchController", ["$scope",
    function($scope) {
        $scope.ctrlScope = $scope;

        $scope.customAction = function($event) {
            if ($scope.Variables.VehicleDiffInvoke.dataSet.vehicle1)
                if ($scope.Variables.VehicleDiffInvoke.dataSet.vehicle1.price)
                    delete $scope.Variables.VehicleDiffInvoke.dataSet.vehicle1.price;

            if ($scope.Variables.VehicleDiffInvoke.dataSet.vehicle2)
                if ($scope.Variables.VehicleDiffInvoke.dataSet.vehicle2.price)
                    delete $scope.Variables.VehicleDiffInvoke.dataSet.vehicle2.price;


            $scope.Variables.VehicleDiffInvoke.update();
        };

    }
]);

Application.$controller("CompareDialogController", ["$scope",
    function($scope) {
        $scope.ctrlScope = $scope;


        $scope.CompareDialogOpened = function($event, $isolateScope) {
            console.log($scope.Variables.VehicleDiffInvoke);
        };

    }
]);

Application.$controller("detailPageDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("gridResponsesController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("PinQuoteDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid5Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("gridUserController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("userPageDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("alertdialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("userResponseDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);