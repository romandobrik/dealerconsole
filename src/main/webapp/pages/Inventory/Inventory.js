Application.$controller("InventoryPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
        //$scope.Widgets.columnDaysInStock.col ng-class = "{'pink':row.daysinstock > 120, 'green':row.daysinstock < 10}";
    };


    $scope.inventoryGridSelect = function($event, $data) {
        console.group("Inventory Item Selected");
        console.log($data);
        //$scope.Variables.MonzaExecutePriceByVin.setInput('vin', $data.vin);

        //$scope.Variables.MonzaExecutePriceByVin.update();

        $scope.Variables.DealerPriceUpsertInvoke.setInput('vin', $data.vin);

        console.log($scope.Variables.DealerPriceUpsertInvoke);

        console.log($scope.Widgets.inventoryGrid.selecteditem);

        console.log($scope.Widgets.locked);

        console.log($scope.Widgets.profitgridcolumn);

        $scope.Variables.profitVariable.dataSet.dataValue = $scope.Widgets.inventoryGrid.selecteditem.uquoteprice - $scope.Widgets.inventoryGrid.selecteditem.invoice;

        if ($scope.Variables.dealerRebateVariable.dataSet.length > 0)
            $scope.Variables.profitVariable.dataSet.dataValue = $scope.Variables.profitVariable.dataSet.dataValue + $scope.Variables.dealerRebateVariable.dataSet[0].rebate;

        console.log($scope.Variables.profitVariable.dataSet.dataValue);

        if ($scope.Variables.profitVariable.dataSet.dataValue.toString().startsWith('-')) {
            console.log($scope.Variables.profitVariable.dataSet.dataValue);
            $scope.Widgets.profitGridrow.setProperty('class', 'pink');
        } else {
            console.log($scope.Variables.profitVariable.dataSet.dataValue);
            $scope.Widgets.profitGridrow.setProperty('class', 'green');
        }

        //$scope.Variables.DealerPriceUpsertInvoke.setInput('state', $data.state);
        //$scope.Widgets.state.widgetProps.defaultvalue = 1;

        console.groupEnd();

    };

    $scope.DealerPriceUpsertInvokeonResult = function(variable, data) {
        console.group("Price Upsert OnResult");
        console.log(data);
        console.groupEnd();
    };


    $scope.DealerPriceUpsertInvokeonSuccess = function(variable, data) {
        console.group("Price Upsert OnSuccess");
        console.log(data);
        $scope.Widgets.inventoryGrid.selecteditem.uquoteprice = data.price;
        $scope.Widgets.inventoryGrid.selecteditem.expiration = data.expiration;
        //$scope.Variables.InventorySearchInvoke.update();
        console.groupEnd();
    };


    $scope.inventoryGridRowdblclick = function($event, $data) {
        console.group("Inventory Row DblClick");
        console.log($data);
        $scope.Variables.selectedVin.dataSet.dataValue = $data.vin;
        console.groupEnd();
    };


    $scope.priceChange = function($event, $isolateScope) {
        console.group("Price Change");
        console.log($isolateScope);
        //$scope.Widgets.netPrice.caption = 10000;
        console.groupEnd();
    };


    $scope.MonzaExecuteSetPriceStateonSuccess = function(variable, data) {
        $scope.Variables.InventorySearchInvoke.update();
    };


    $scope.MonzaExecuteSetPriceLockedonSuccess = function(variable, data) {
        $scope.Variables.InventorySearchInvoke.update();
    };


    $scope.MonzaExecutePricesReleaseonSuccess = function(variable, data) {
        $scope.Variables.InventorySearchInvoke.update();
    };


    $scope.MonzaExecutePricesOnHoldonSuccess = function(variable, data) {
        $scope.Variables.InventorySearchInvoke.update();
    };



    $scope.refreshButtonClick = function($event, $isolateScope) {
        $scope.Variables.InventorySearchInvoke.update();
    };



    $scope.financeChange = function($event, $isolateScope) {
        console.group("Finance Change");
        console.log($scope.Widgets.finance);
        console.log($scope.Widgets.financeDiscount);
        $scope.Widgets.financeDiscount.readonly = false;
        console.groupEnd();

    };


    $scope.tradeinChange = function($event, $isolateScope) {
        $scope.Widgets.tradeinDiscount.readonly = false;
    };


}]);


Application.$controller("inventoryGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.lockedAction = function(selectedItemData) {
            console.group("Locked Action");
            console.log(selectedItemData);

            var locked = selectedItemData.locked;

            if (locked === 0)
                locked = 1;
            else
                locked = 0;

            $scope.Variables.MonzaExecuteSetPriceLocked.setInput('vin', selectedItemData.vin);
            $scope.Variables.MonzaExecuteSetPriceLocked.setInput('locked', locked);
            $scope.Variables.MonzaExecuteSetPriceLocked.update();
            console.groupEnd();
        };

    }
]);

/*
Application.$controller("uQuotePriceFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.customAction = function($event) {
            console.group("uQuote new price");
            console.log($scope.Widgets.uQuotePriceForm.formFields);
            $scope.Variables.DealerPriceUpsertInvoke.update();
            $scope.Variables.MonzaExecutePriceByVin.update();
            console.log($scope.Variables.DealerPriceUpsertInvoke);
            console.log($scope.Variables.MonzaExecutePriceByVin);

            $scope.Widgets.inventoryGrid.selecteditem.uquoteprice = $scope.Variables.DealerPriceUpsertInvoke.dataSet.price;

            console.groupEnd();
        };

    }
]);
*/

Application.$controller("dealConfirmDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("repriceAllDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("detailPageDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("liveform1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);


Application.$controller("confirmReleaseDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.confirmReleaseDialogOk = function($event, $isolateScope) {
            $scope.Variables.MonzaExecutePricesRelease.update();
        };

    }
]);

Application.$controller("confirmInPrepDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.confirmOnHoldDialogOk = function($event, $isolateScope) {
            $scope.Variables.MonzaExecutePricesOnHold.update();
        };

    }
]);

Application.$controller("emailDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("gridModelsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);