Application.$controller("ConfigurationPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };




    $scope.grid1Select = function($event, $rowData) {
        $scope.Variables.selectedMake.dataSet.dataValue = $rowData.make.toLowerCase();
        $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Variables.selectedMake.dataSet.dataValue);
        $scope.Variables.modelsVariable.update();
    };

}]);


Application.$controller("livefilter1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, rowData) {
            if (rowData.make !== null && rowData.year !== null) {

                $scope.Variables.selectedMake.dataSet.dataValue = rowData.make.toLowerCase();

                $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Variables.selectedMake.dataSet.dataValue);
                $scope.Variables.modelsVariable.update();
            }
        };

        $scope.makeChange = function($event, $isolateScope, rowData) {
            console.group("MonzaPricesSetupMakeChange");
            console.log(rowData);
            console.log($isolateScope._proxyModel);
            $scope.Variables.selectedMake.dataSet.dataValue = $isolateScope._proxyModel.toLowerCase();
            $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Variables.selectedMake.dataSet.dataValue);

            $scope.Variables.modelsVariable.update();
            console.log($scope.Variables.modelsVariable);
            console.groupEnd();
        };



    }
]);

Application.$controller("liveform1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.makeChange = function($event, $isolateScope) {
            console.group("MonzaConfigurationMakeChange");
            $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Widgets.make.datavalue.toLowerCase());
            $scope.Variables.modelsVariable.update();
            console.log($scope.Variables.modelsVariable);
            console.groupEnd();
        };

    }
]);