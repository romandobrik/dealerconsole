Application.$controller("RepriceAllPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        console.group("RepriceAllPageReady");

        console.log($scope.$parent.Widgets.selectChannel);
        $scope.Variables.channelId.dataSet.dataValue = $scope.$parent.Widgets.selectChannel.datavalue;
        console.log($scope.Variables.channelId);

        console.groupEnd();
    };


    $scope.RepriceAllVariableonSuccess = function(variable, data) {
        console.group("Reprice All OnSuccess");
        console.log(data);
        $scope.$parent.Variables.InventorySearchInvoke.update();
        console.groupEnd();
    };


    $scope.makeChange = function($event, $isolateScope) {
        console.group("RepriceMakeChange");

        $scope.Variables.modelsRepriceVariable.setInput('makeNiceName', $scope.Widgets.make.datavalue.toLowerCase());
        $scope.Variables.modelsRepriceVariable.update();
        console.log($scope.Variables.modelsRepriceVariable);
        console.groupEnd();
    };


    $scope.modelChange = function($event, $isolateScope) {

    };

}]);

Application.$controller("grid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid3Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);