Application.$controller("LeadsScansPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.$on('PUBNUB_RECEIVED', function(event, data) {

        if (data.message.event == 'CAR_SCAN')
            $scope.Variables.DealerScansInvoke.update();

        console.log(data);
    });

    $scope.gridScansSelect = function($event, $data) {
        rewrite($data);
    };


    function rewrite($data) {
        console.group("Rewrite detail");
        console.log($data);

        $scope.Variables.selectedZipcode.dataSet.dataValue = $data.zipcode;

        $scope.Variables.VehicleDescriptionInvoke.setInput('vin', $data.vin);
        $scope.Variables.VehicleDescriptionInvoke.setInput('zipcode', $data.zipcode);

        $scope.Variables.VehicleDescriptionInvoke.update();

        $scope.Variables.imageUrlVariable.dataSet.url = $data.image;

        console.log($scope.Variables.imageUrlVariable);
        $scope.Variables.mapDataVariable.dataSet.lon = $data.longitude;
        $scope.Variables.mapDataVariable.dataSet.lat = $data.latitude;
        $scope.Variables.mapDataVariable.dataSet.info = $data.make + " " + $data.model + " " + $data.trim;

        $scope.Widgets.vehicle.caption = $data.year + " " + $data.make + " " + $data.model + " " + $data.trim;

        $scope.Widgets.popoverColor.caption = "Exterior:" + $data.color;
        $scope.Variables.colorVariable.dataSet.dataValue = "#" + $data.colorid;
        $scope.Variables.msrpVariable.dataSet.dataValue = $data.msrp;
        $scope.Widgets.popoverDealer.caption = $data.dealername;
        $scope.Widgets.labelVin.caption = "VIN:" + $data.vin;

        if ($data.channel === 0)
            $scope.Widgets.channelLabel.caption = "Competing";
        else if ($data.channel === 1)
            $scope.Widgets.channelLabel.caption = "Direct";
        else if ($data.channel === 2)
            $scope.Widgets.channelLabel.caption = "Internet";
        else
            $scope.Widgets.channelLabel.caption = "";

        if ($data.make === "Ford" || $data.make === "Lincoln") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.inventory.ford.com/services/inventory/WindowSticker.pdf?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else if ($data.make === "Hyundai") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.stocktonhyundai.com/external-catalog-services/rest/monroney/windowsticker?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else if ($data.make === "Chrysler" || $data.make === "Dodge" || $data.make === "Ram" || $data.make === "Jeep") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.chrysler.com/hostd/windowsticker/getWindowStickerPdf.do?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else if ($data.make === "Infiniti") {
            $scope.Widgets.anchorSticker.hyperlink = "http://www.infinitiusa.com/buildyourinfiniti/inventory/windowSticker?vin=" + $data.vin;
            $scope.Widgets.anchorSticker.show = true;
        } else
            $scope.Widgets.anchorSticker.show = false;

        console.groupEnd();

    }

    function contains(a, obj) {
        var i = a.length;
        while (i--) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    }



    $scope.gridInventoryMatchSelect = function($event, $data) {
        console.log($data);
        $scope.Variables.VehicleDiffInvoke.setInput('vin2', $data.vin);
        $scope.Variables.VehicleDiffInvoke.setInput('zipcode2', $data.zipcode);
    };


    $scope.gridInventoryMatchRowdblclick = function($event, $data) {
        $scope.Variables.selectedVin.dataSet.dataValue = $data.vin;

        //$scope.Variables.selectedZipcode.dataSet.dataValue = Variables.MonzaDealersData.dataSet.data[0].zipcode;
    };



    $scope.gridScansRowdblclick = function($event, $data) {
        $scope.Variables.selectedVin.dataSet.dataValue = $data.vin;
        $scope.Variables.selectedZipcode.dataSet.dataValue = $data.zipcode;
    };



    $scope.VehicleDescriptionInvokeonSuccess = function(variable, data) {
        console.group("VehicleDescriptionInvokeonSuccess");
        console.log(data);
        $scope.Variables.VehiclePriceVariable.dataSet.dataValue = data.price;
        $scope.Variables.VehicleEngineVariable.dataSet.dataValue = data.engine;
        $scope.Variables.VehicleDealerVariable.dataSet.dataValue = data.dealer;
        $scope.Variables.VehicleImagesVariable.dataSet.dataValue = data.imagelist;
        $scope.Variables.VehicleInteriorVariable.dataSet.dataValue = data.interiorColor;
        console.log($scope.Variables.VehicleInteriorVariable);
        console.groupEnd();
    };


    $scope.UserLeadVariableonSuccess = function(variable, data) {
        console.group("UserLeadVariableonSuccess");
        console.log(data);

        if (typeof data !== 'undefined' && typeof data.userid !== 'undefined')
            $scope.Variables.selectedUser.dataSet.dataValue = data.userid.toUpperCase();

        if (typeof data !== 'undefined' && typeof data.profile_pic !== 'undefined') {
            $scope.Widgets.messageAnchor.show = true;

        } else {
            $scope.Widgets.messageAnchor.show = true;
        }

        console.groupEnd();
    };

}]);


Application.$controller("gridUserController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("gridResponsesController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("gridScansController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("gridInventoryMatchController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("CompareDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("detailPageDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("PinQuoteDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("userResponseDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);