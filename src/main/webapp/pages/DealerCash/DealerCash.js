Application.$controller("DealerCashPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.grid2Select = function($event, $rowData) {
        console.group("grid2Select");

        if ($rowData.make !== null && $rowData.year !== null) {
            $scope.Variables.selectedYear.dataSet.dataValue = $rowData.year;
            $scope.Variables.selectedMake.dataSet.dataValue = $rowData.make.toLowerCase();
            $scope.Variables.modelsVariable.setInput('year', $scope.Variables.selectedYear.dataSet.dataValue);
            $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Variables.selectedMake.dataSet.dataValue);
            $scope.Variables.modelsVariable.update();
        }
        console.log($scope.Variables.modelsVariable);
        console.groupEnd();
    };

}]);




Application.$controller("grid2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.makeChange = function($event, $isolateScope, rowData) {
            console.group("MonzaPricesSetupMakeChange");
            console.log(rowData);
            console.log($isolateScope._proxyModel);
            $scope.Variables.selectedMake.dataSet.dataValue = $isolateScope._proxyModel.toLowerCase();
            $scope.Variables.modelsVariable.setInput('year', $scope.Variables.selectedYear.dataSet.dataValue);
            $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Variables.selectedMake.dataSet.dataValue);

            $scope.Variables.modelsVariable.update();
            console.log($scope.Variables.modelsVariable);
            console.groupEnd();
        };


        $scope.yearChange = function($event, $isolateScope, rowData) {
            console.group("MonzaPricesSetupYearChange");
            console.log(rowData);
            console.log($isolateScope);
            $scope.Variables.selectedYear.dataSet.dataValue = $isolateScope._proxyModel;
            $scope.Variables.modelsVariable.setInput('year', $scope.Variables.selectedYear.dataSet.dataValue);
            $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Variables.selectedMake.dataSet.dataValue);
            $scope.Variables.modelsVariable.update();
            console.log($scope.Variables.modelsVariable);
            console.groupEnd();
        };

        $scope.updaterowAction = function($event, $rowData) {
            console.group("updaterowAction");
            console.log($scope.Variables.loggedInUser);

            //$scope.Widgets.modifiedby.datatvalue = $scope.Variables.loggedInUser.dataSet.id;
            //$scope.Variables.MonzaPriceSetupData.setInput('modifiedby', $scope.Variables.loggedInUser.dataSet.id);


            if ($rowData.make !== null && $rowData.year !== null) {
                $scope.Variables.selectedYear.dataSet.dataValue = $rowData.year;
                $scope.Variables.selectedMake.dataSet.dataValue = $rowData.make.toLowerCase();
                $scope.Variables.modelsVariable.setInput('year', $scope.Variables.selectedYear.dataSet.dataValue);
                $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Variables.selectedMake.dataSet.dataValue);
                $scope.Variables.modelsVariable.update();
            }

            //console.log($scope.Variables.MonzaPriceSetupData);
            console.groupEnd();
        };

    }
]);

Application.$controller("liveform1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.yearChange = function($event, $isolateScope) {
            console.group("DealerCashSetupMakeChange");
            $scope.Variables.modelsVariable.setInput('year', $scope.Widgets.year.datavalue);
            $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Widgets.make.datavalue.toLowerCase());
            //$scope.Variables.modelsVariable.setInput('makeNiceName', 'hyundai');
            $scope.Variables.modelsVariable.update();
            console.log($scope.Variables.modelsVariable);
            console.groupEnd();

        };


        $scope.makeChange = function($event, $isolateScope) {
            console.group("DealerCashSetupMakeChange");
            $scope.Variables.modelsVariable.setInput('year', $scope.Widgets.year.datavalue);
            $scope.Variables.modelsVariable.setInput('makeNiceName', $scope.Widgets.make.datavalue.toLowerCase());
            //$scope.Variables.modelsVariable.setInput('makeNiceName', 'hyundai');
            $scope.Variables.modelsVariable.update();
            console.log($scope.Variables.modelsVariable);
            console.groupEnd();

        };

    }
]);