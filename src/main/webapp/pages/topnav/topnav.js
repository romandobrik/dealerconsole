Application.$controller("topnavPageController", ["$scope", function($scope) {
    "use strict";

    $scope.$on('PUBNUB_RECEIVED', function(event, data) {
        //$scope.Widgets.messageStatus.show = true;
        console.group("Received Pubnub event");
        console.log(data);
        if (data.message.event == 'CAR_QUOTE_REQUEST')
            $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Quote requested for ' + data.message.payload.make + ' ' + data.message.payload.model + ' ' + data.message.payload.trim;
        else if (data.message.event == 'CAR_SCAN')
            $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Vehicle scanned ' + data.message.payload.make + ' ' + data.message.payload.model + ' ' + data.message.payload.trim;
        else if (data.message.event == 'CAR_QUOTE_ACCEPTED') {
            $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Offer accepted for ' + data.message.payload.make + ' ' + data.message.payload.model + ' ' + data.message.payload.trim;
        } else if (data.message.event == 'CAR_QUOTE_CONFIRM') {
            if (data.message.properties.state == 'ACCEPTED')
                $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Response accepted';
            else if (data.message.properties.state == 'CANCELED')
                $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Response canceled';
        } else if (data.message.event == 'CAR_QUOTE_RESPONSE_CONFIRM') {
            $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Response confirmed';
        } else if (data.message.event == 'CAR_QUOTE_CANCEL_CONFIRM') {
            $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Cancelation confirmed';
        } else
            $scope.Variables.infoMessageVariable.dataSet.dataValue = 'Unknown message';

        console.log($scope.Variables.infoMessageVariable);
        $scope.Widgets.messageStatus.show = true;
        console.groupEnd();
    });

}]);