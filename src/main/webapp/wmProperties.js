var _WM_APP_PROPERTIES = {
  "activeTheme" : "cool-blue",
  "defaultLanguage" : "en",
  "displayName" : "DealerConsole",
  "homePage" : "Main",
  "name" : "DealerConsole",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};